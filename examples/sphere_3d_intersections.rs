use ambient_geometry::{RayBundle, Sphere};
use image::{DynamicImage, GenericImage, Pixel, Rgba};

pub fn main() {
    let width: usize = 400;
    let height: usize = 400;

    let mut image = DynamicImage::new_rgb8(width as u32, height as u32);
    let black = Rgba::from_channels(0, 0, 0, 0);
    let green = Rgba::from_channels(0, 255, 0, 0);

    let s = Sphere::from(([0.0, 0.0, -5.0], 1.0));
    let rb = RayBundle::standard(width, height);
    let res = rb.get_intersections(&s);
    for h in 0..width {
        for w in 0..height {
            let r = res.rays()[h * width + w];
            if r.dir().magnitude() != &0.0 {
                image.put_pixel(w as u32, h as u32, green);
            } else {
                image.put_pixel(w as u32, h as u32, black);
            }
        }
    }

    image
        .save("./examples/sphere_3d_intersections.rs.jpg")
        .expect("Failed saving image");
}
