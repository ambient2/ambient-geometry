use ambient_geometry::{
    Color, DirectedLight, Point, Ray, RayBundle, Sphere, Surface, Vector, VisibleObject,
};
use image::{GenericImage, Pixel, Rgba};

pub fn main() {
    let rb = RayBundle::standard(800, 800);
    let sphere: Sphere<3> = Sphere::new(Point::from([0.0, 0.0, -5.0]), 1.0);
    let v_sphere: VisibleObject<Sphere<3>, 3> =
        VisibleObject::new(sphere, Color::new(1.0, 0.0, 0.0), 0.5);

    let light = DirectedLight::new(
        Point::from([0.0, 5.0, -5.0]),
        Vector::from([0.0, -1.0, 0.0]),
        Color::new(1.0, 1.0, 1.0),
        1.0,
    );

    let mut image = image::DynamicImage::new_rgb8(800, 800);
    let black: Rgba<u8> = Rgba::from_channels(0, 0, 0, 0);
    let results = rb.get_intersections(v_sphere.surface());

    for h in 0..800 {
        for w in 0..800 {
            let r = results.rays()[h * 800 + w];
            if r.dir().magnitude() != &0.0 {
                let hp = r.origin().clone();
                let light_intersected = v_sphere
                    .surface()
                    .intersect(&Ray::new(hp.clone(), light.direction().clone()));
                if light_intersected.is_none() {
                    image.put_pixel(w as u32, h as u32, black);
                } else {
                    let light_alignment = v_sphere
                        .surface()
                        .norm(&hp)
                        .unwrap_or(Vector::default())
                        .dot(light.direction());
                    if light_alignment > 1e-8 {
                        let light_power = light_alignment * light.intensity();

                        let light_reflected = v_sphere.albedo() / std::f64::consts::PI;
                        let light_color = light.color().scale(light_power.abs() * light_reflected);
                        image.put_pixel(
                            w as u32,
                            h as u32,
                            light_color.mix(v_sphere.color()).to_rgba(),
                        );
                    } else {
                        image.put_pixel(w as u32, h as u32, black);
                    }
                }
            } else {
                image.put_pixel(w as u32, h as u32, black);
            }
        }
    }

    image
        .save("./examples/sphere_3d_ray_traced.rs.jpg")
        .expect("Failed saving image");
}
