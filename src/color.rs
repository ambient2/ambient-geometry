use image::Pixel;

const GAMMA: f64 = 2.2;
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Color {
    red: f64,
    blue: f64,
    green: f64,
}

impl Color {
    pub fn to_rgba(&self) -> image::Rgba<u8> {
        image::Rgba::from_channels(
            (gamma_encode(self.red) * 255.0) as u8,
            (gamma_encode(self.green) * 255.0) as u8,
            (gamma_encode(self.blue) * 255.0) as u8,
            255,
        )
    }

    pub fn new(red: f64, blue: f64, green: f64) -> Color {
        Color { red, blue, green }
    }

    pub fn scale(&self, factor: f64) -> Color {
        let Color { red, blue, green } = self;
        Color {
            red: red * factor,
            blue: blue * factor,
            green: green * factor,
        }
    }

    pub fn clamp(&self) -> Color {
        Color {
            red: self.red.min(1.0).max(0.0),
            blue: self.blue.min(1.0).max(0.0),
            green: self.green.min(1.0).max(0.0),
        }
    }

    pub fn mix(&self, color: &Color) -> Color {
        let r = self.red * color.red;
        let b = self.blue * color.blue;
        let g = self.green * color.green;
        Color::new(r, b, g)
    }
}

fn gamma_encode(linear: f64) -> f64 {
    linear.powf(1.0 / GAMMA)
}
