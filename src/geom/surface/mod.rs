mod sphere;
pub use sphere::Sphere;
mod plane;
pub use plane::Plane;

use super::point::Point;
use super::ray::Ray;
use super::vector::Vector;

pub trait Surface<const N: usize> {
    fn intersect(&self, ray: &Ray<N>) -> Option<Point<N>>;
    fn norm(&self, at: &Point<N>) -> Option<Vector<N>>;
}

pub struct CompositeSurface<const N: usize> {
    constituents: Vec<Box<dyn Surface<N>>>,
}

impl<const N: usize> CompositeSurface<N> {
    pub fn new(constituents: Vec<Box<dyn Surface<N>>>) -> CompositeSurface<N> {
        CompositeSurface { constituents }
    }

    pub fn constituents(&self) -> &[Box<dyn Surface<N>>] {
        &self.constituents
    }
}

impl<const N: usize> Surface<N> for CompositeSurface<N> {
    fn intersect(&self, ray: &Ray<N>) -> Option<Point<N>> {
        for b in self.constituents() {
            let r = b.intersect(ray);
            if r.is_some() {
                return r;
            }
        }
        None
    }

    fn norm(&self, at: &Point<N>) -> Option<Vector<N>> {
        for b in self.constituents() {
            let r = b.norm(at);
            if r.is_some() {
                return r;
            }
        }
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn composite_surface_accepts_surfaces_diff_types() {
        let s: Sphere<3> = Sphere::new(Point::default(), 1.0);
        let p = Point::from([0.0, 0.0, -5.0]);
        let cs = CompositeSurface::new(vec![Box::from(s), Box::from(p)]);
        assert_eq!(2, cs.constituents().len())
    }

    #[test]
    fn composite_surface_detects_intersections_on_both() {
        let s1: Sphere<3> = Sphere::new(Point::from([3.0, 0.0, 3.0]), 1.0);
        let s2: Sphere<3> = Sphere::new(Point::from([-3.0, 0.0, 3.0]), 1.0);
        let r1 = Ray::from((Point::default(), Vector::from([1.0, 0.0, 1.0])));
        let r2 = Ray::from((Point::default(), Vector::from([-1.0, 0.0, 1.0])));

        let cs = CompositeSurface::new(vec![Box::from(s1), Box::from(s2)]);
        let i1 = cs.intersect(&r1);
        let i2 = cs.intersect(&r2);
        assert!(i1.is_some());
        assert!(i2.is_some());
    }
}
