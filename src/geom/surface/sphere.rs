use super::super::point::Point;
use super::super::ray::Ray;
use super::super::vector::Vector;
use super::Surface;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Sphere<const N: usize> {
    center: Point<N>,
    radius: f64,
}

impl<const N: usize> Surface<N> for Sphere<N> {
    fn intersect(&self, ray: &Ray<N>) -> Option<Point<N>> {
        let ray = ray.normalize();
        let p_min_c = ray.origin().clone() - self.center.clone();
        let dir_dot_p_min_c = Vector::from(p_min_c).dot(ray.dir());
        let dir_dot_p_sqrd = dir_dot_p_min_c.powi(2);

        let p_min_c_sqrd = Vector::from(p_min_c.clone()).dot(&Vector::from(p_min_c.clone()));
        let p_min_c_sqrd_min_r_sqrd = p_min_c_sqrd - self.radius.powi(2);

        let discr = dir_dot_p_sqrd - p_min_c_sqrd_min_r_sqrd;

        if discr < 0.0 {
            None
        } else if discr == 0.0 {
            let scaled_dir = ray.dir().scale(-dir_dot_p_min_c);
            let p = Point::from(scaled_dir.components().clone());
            Some(p)
        } else {
            let scaled_dir = ray.dir().scale(-dir_dot_p_min_c);
            let v1 = ray.dir().normalize().scale(discr.sqrt());
            let cand1 = scaled_dir.clone() - v1.clone();
            let cand2 = scaled_dir.clone() + v1;

            let lft = ray.dir().dot(&cand1);
            let rght = ray.dir().dot(&cand2);
            if lft < rght {
                let comps = Point::from(cand1.components().clone());
                Some(comps)
            } else {
                let comps = Point::from(cand2.components().clone());
                Some(comps)
            }
        }
    }

    fn norm(&self, at: &Point<N>) -> Option<super::Vector<N>> {
        let p = at.clone() - self.center().clone();

        let v = Vector::from(p);
        if (v.magnitude() - self.radius).abs() <= 1e-8 {
            Some(v.normalize())
        } else {
            None
        }
    }
}

impl<const N: usize> Sphere<N> {
    pub fn new(center: Point<N>, radius: f64) -> Sphere<N> {
        Sphere { center, radius }
    }

    fn center(&self) -> &Point<N> {
        &self.center
    }
}

impl<const N: usize> From<([f64; N], f64)> for Sphere<N> {
    fn from((cs, r): ([f64; N], f64)) -> Self {
        Sphere::new(Point::from(cs), r)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sphere_surface_impl_intersects_returns_some_when_one_intersect() {
        let s = Sphere::from(([1.0, 1.0], 1.0));
        let r = Ray::from((Point::default(), Vector::from([1.0, 0.0])));
        let res = s.intersect(&r);
        assert_eq!(Some(Point::from([1.0, 0.0])), res)
    }

    #[test]
    fn sphere_surface_impl_intersects_returns_some_closer_when_two_intersect() {
        let s = Sphere::from(([2.0, 0.0], 1.0));
        let r = Ray::from((Point::default(), Vector::from([1.0, 0.0])));
        let res = s.intersect(&r);
        assert_eq!(Some(Point::from([1.0, 0.0])), res)
    }

    #[test]
    fn sphere_surface_impl_intersects_returns_none_when_ray_wrong_dir() {
        let s = Sphere::from(([2.0, 0.0], 1.0));
        let r = Ray::from((Point::default(), Vector::from([-1.0, -1.0])));
        let res = s.intersect(&r);
        assert_eq!(None, res)
    }

    #[test]
    fn sphere_surface_impl_norm_returns_some_when_point_on_surface() {
        let s = Sphere::from(([2.0, 0.0], 1.0));
        let p = Point::from([1.0, 0.0]);
        let v = s.norm(&p);
        assert_eq!(Some(Vector::from([-1.0, 0.0])), v)
    }

    #[test]
    fn sphere_surface_impl_norm_returns_none_when_point_not_on_surface() {
        let s = Sphere::from(([2.0, 0.0], 1.0));
        let p = Point::from([0.0, 0.0]);
        let v = s.norm(&p);
        assert_eq!(None, v)
    }
}
