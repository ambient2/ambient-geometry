use super::super::point::Point;
use super::super::vector::Vector;
use super::Surface;

#[derive(Debug, Clone, PartialEq)]
pub struct Plane<const N: usize> {
    origin: Point<N>,
    normal: Vector<N>,
}

impl<const N: usize> Plane<N> {
    pub fn new(origin: Point<N>, normal: Vector<N>) -> Plane<N> {
        Plane { origin, normal }
    }

    pub fn origin(&self) -> &Point<N> {
        &self.origin
    }

    pub fn normal(&self) -> &Vector<N> {
        &self.normal
    }
}

impl<const N: usize> Surface<N> for Plane<N> {
    fn intersect(&self, ray: &super::Ray<N>) -> Option<Point<N>> {
        let denom = self.normal().dot(ray.dir());
        if denom.abs() <= 1e-8 {
            return None;
        } else {
            let orig = self.origin().clone();
            let p0 = ray.origin().clone();
            let num = (Vector::from(orig) - Vector::from(p0)).dot(self.normal());
            let scale = num / denom;
            let vec_scale = ray.dir().scale(scale);
            let res_vec = vec_scale + Vector::from(ray.origin().clone());
            Some(Point::from(res_vec.components().clone()))
        }
    }

    fn norm(&self, at: &Point<N>) -> Option<Vector<N>> {
        let os = self.origin().clone() - at.clone();
        let v = Vector::from(os);
        let res = v.dot(self.normal());
        if res.abs() <= 1e-8 {
            Some(self.normal().clone())
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use super::super::Ray;
    use super::*;

    #[test]
    fn plane_normal_returns_some_for_point_on_plane() {
        let p = Plane::new(Point::default(), Vector::from([1.0, 0.0, 0.0]));
        let point = Point::from([0.0, 10.0, -1.0]);
        let r = p.norm(&point);
        assert_eq!(Some(Vector::from([1.0, 0.0, 0.0])), r);
    }

    #[test]
    fn plane_normal_returns_none_for_point_off_plane() {
        let p = Plane::new(Point::default(), Vector::from([1.0, 0.0, 0.0]));
        let point = Point::from([1.0, 10.0, -1.0]);
        let r = p.norm(&point);
        assert_eq!(None, r);
    }

    #[test]
    fn plane_intersect_returns_none_for_parallel_ray() {
        let p = Plane::new(Point::default(), Vector::from([1.0, 0.0, 0.0]));
        let v = Vector::from([0.0, 1.0, 1.0]).normalize();
        let r = Ray::new(Point::from([0.0, 0.0, 1.0]), v);

        let res = p.intersect(&r);
        assert_eq!(None, res);
    }

    #[test]
    fn plane_intersect_returns_some_for_non_parallel_ray() {
        let p = Plane::new(Point::default(), Vector::from([1.0, 0.0, 0.0]));
        let v = Vector::from([1.0, 1.0, 1.0]).normalize();
        let r = Ray::new(Point::from([-1.0, 0.0, 0.0]), v);

        let res = p.intersect(&r);
        assert!(res.is_some());
    }
}
