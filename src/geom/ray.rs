use super::point::Point;
use super::surface::Surface;
use super::vector::Vector;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Ray<const N: usize> {
    origin: Point<N>,
    dir: Vector<N>,
}

impl<const N: usize> Ray<N> {
    pub fn new(origin: Point<N>, dir: Vector<N>) -> Ray<N> {
        let dir = dir.normalize();
        Ray { origin, dir }
    }

    pub fn origin(&self) -> &Point<N> {
        &self.origin
    }

    pub fn dir(&self) -> &Vector<N> {
        &self.dir
    }

    pub fn scale(&self, factor: f64) -> Ray<N> {
        let origin = self.origin().clone();
        let dir = self.dir().scale(factor);
        Ray::new(origin, dir)
    }

    pub fn normalize(&self) -> Ray<N> {
        let origin = self.origin().clone();
        let dir = self.dir().normalize();
        Ray::new(origin, dir)
    }
}

impl<const N: usize> From<(Point<N>, Vector<N>)> for Ray<N> {
    fn from(cs: (Point<N>, Vector<N>)) -> Self {
        Ray::new(cs.0, cs.1)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct RayBundle<const N: usize> {
    rays: Vec<Ray<N>>,
}

impl<const N: usize> From<(Point<N>, Vec<Point<N>>)> for RayBundle<N> {
    fn from((pt, tos): (Point<N>, Vec<Point<N>>)) -> Self {
        let rays = tos
            .iter()
            .map(|p| Vector::from(p.clone()).normalize())
            .map(|v| Ray::new(pt.clone(), v))
            .collect();

        RayBundle { rays }
    }
}

impl RayBundle<3> {
    pub fn standard(width: usize, height: usize) -> RayBundle<3> {
        let source = Point::default();
        let width_step = 2.0 / width as f64;
        let height_step = 2.0 / height as f64;
        let mut ps = Vec::new();
        for h in 0..width {
            for w in 0..height {
                let x = -1.0 + w as f64 * width_step;
                let y = -1.0 + h as f64 * height_step;
                let z = -1.0;
                ps.push(Point::from([x, y, z]));
            }
        }
        RayBundle::from((source, ps))
    }
}

impl<const N: usize> RayBundle<N> {
    pub fn get_intersections<T>(&self, surface: &T) -> RayBundle<N>
    where
        T: Surface<N>,
    {
        let mut rays: Vec<Ray<N>> = Vec::new();
        for r in self.rays.iter() {
            let intersects = surface.intersect(r);
            match intersects {
                Some(p) => {
                    let norm = surface.norm(&p).expect("Norm expected");
                    rays.push(Ray::from((p, norm)));
                }
                None => {
                    rays.push(Ray::from((
                        r.origin().clone(),
                        Vector::from(Point::default()),
                    )));
                }
            }
        }
        RayBundle { rays }
    }

    pub fn rays(&self) -> &[Ray<N>] {
        &self.rays
    }
}
