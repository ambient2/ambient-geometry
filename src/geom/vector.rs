use std::ops::{Add, Neg, Sub};

use super::point::Point;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vector<const N: usize> {
    components: [f64; N],
    magnitude: f64,
}

impl<const N: usize> Vector<N> {
    pub fn magnitude(&self) -> &f64 {
        &self.magnitude
    }

    pub fn components(&self) -> &[f64; N] {
        &self.components
    }

    pub fn dot(&self, rhs: &Vector<N>) -> f64 {
        let mut res = 0.0;
        for (lc, rc) in self.components().iter().zip(rhs.components()) {
            res += lc * rc;
        }

        res
    }

    pub fn parallel(&self, rhs: &Vector<N>) -> bool {
        let dot_prod = self.normalize().dot(&rhs.normalize());
        if dot_prod == 0.0 {
            false
        } else {
            (dot_prod - 1.0).abs() <= 1e-8
        }
    }

    pub fn normalize(&self) -> Vector<N> {
        let norm_squared = self.dot(&self);
        if norm_squared == 0.0 {
            Vector::default()
        } else {
            let norm = norm_squared.sqrt();
            let mut newcs = [0.0; N];
            for (ix, c) in self.components().iter().enumerate() {
                newcs[ix] = c / norm;
            }

            Vector::from(newcs)
        }
    }

    pub fn scale(&self, factor: f64) -> Vector<N> {
        let mut comps = [0.0; N];
        for (ix, c) in self.components().iter().enumerate() {
            comps[ix] = c * factor;
        }

        Vector::from(comps)
    }
}

impl<const N: usize> Default for Vector<N> {
    fn default() -> Self {
        Self {
            components: [0.0; N],
            magnitude: 0.0,
        }
    }
}

impl<const N: usize> From<[f64; N]> for Vector<N> {
    fn from(components: [f64; N]) -> Self {
        let mut squared_magnitude = 0.0;
        for c in components {
            squared_magnitude += c * c;
        }
        if squared_magnitude == 0.0 {
            Vector {
                components: components,
                magnitude: 0.0,
            }
        } else {
            Vector {
                components: components,
                magnitude: squared_magnitude.sqrt(),
            }
        }
    }
}

impl<const N: usize> From<Point<N>> for Vector<N> {
    fn from(ps: Point<N>) -> Self {
        let components = ps.components().to_owned();
        Vector::from(components)
    }
}

impl<const N: usize> Add for Vector<N> {
    type Output = Vector<N>;

    fn add(self, rhs: Self) -> Self::Output {
        let mut newcs = [0.0; N];
        for (ix, (lc, rc)) in self.components().iter().zip(rhs.components()).enumerate() {
            newcs[ix] = lc + rc;
        }
        Vector::from(newcs)
    }
}

impl<const N: usize> Sub for Vector<N> {
    type Output = Vector<N>;

    fn sub(self, rhs: Self) -> Self::Output {
        let mut newcs = [0.0; N];
        for (ix, (lc, rc)) in self.components().iter().zip(rhs.components()).enumerate() {
            newcs[ix] = lc - rc;
        }
        Vector::from(newcs)
    }
}

impl<const N: usize> Neg for Vector<N> {
    type Output = Vector<N>;

    fn neg(self) -> Self::Output {
        let mut newcs = [0.0; N];
        for (ix, c) in self.components().iter().enumerate() {
            newcs[ix] = -c;
        }
        Vector::from(newcs)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn vector_defaults_to_zero_vector() {
        let v: Vector<3> = Vector::default();
        let exp = Vector::from([0.0, 0.0, 0.0]);

        assert_eq!(exp, v);
    }

    #[test]
    fn vector_magnitude_correct_for_default() {
        let v: Vector<3> = Vector::default();
        assert_eq!(&0.0, v.magnitude());
    }

    #[test]
    fn vector_adds_componentwise() {
        let v1 = Vector::from([1.0, 2.0, 3.0]);
        let v2 = Vector::from([2.0, 2.0, 2.0]);
        let r = v1 + v2;

        let expected = Vector::from([3.0, 4.0, 5.0]);
        assert_eq!(expected, r);
    }

    #[test]
    fn vector_subs_componentwise() {
        let v1 = Vector::from([1.0, 2.0, 3.0]);
        let v2 = Vector::from([2.0, 2.0, 2.0]);
        let r = v1 - v2;

        let expected = Vector::from([-1.0, 0.0, 1.0]);
        assert_eq!(expected, r);
    }

    #[test]
    fn vector_negs_componentwise() {
        let v1 = Vector::from([1.0, 2.0, 3.0]);
        let r = -v1;

        let expected = Vector::from([-1.0, -2.0, -3.0]);
        assert_eq!(expected, r);
    }

    #[test]
    fn vector_dot_product_computes_correctly() {
        let v1 = Vector::from([1.0, 2.0, 3.0]);
        let v2 = Vector::from([2.0, 2.0, 2.0]);
        let res = v1.dot(&v2);
        assert_eq!(12.0, res);
    }

    #[test]
    fn vector_dot_product_orthogonal_is_zero() {
        let v1 = Vector::from([0.5, 0.5]);
        let v2 = Vector::from([-0.5, 0.5]);
        let res = v1.dot(&v2);
        assert_eq!(0.0, res);
    }
}
