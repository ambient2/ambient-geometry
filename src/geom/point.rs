use std::ops::{Add, Mul, Neg, Sub};

use super::surface::Surface;
use super::vector::Vector;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Point<const N: usize> {
    components: [f64; N],
}

impl<const N: usize> Point<N> {
    pub fn components(&self) -> &[f64; N] {
        &self.components
    }
}

impl<const N: usize> Surface<N> for Point<N> {
    fn intersect(&self, ray: &super::ray::Ray<N>) -> Option<Point<N>> {
        let point = self.to_owned() - ray.origin().to_owned();
        if point == Point::default() {
            return Some(self.clone());
        }
        let v = Vector::from(point).normalize();
        if v.parallel(ray.dir()) {
            Some(self.clone())
        } else {
            None
        }
    }

    fn norm(&self, at: &Point<N>) -> Option<super::vector::Vector<N>> {
        if at == self {
            Some(Vector::default())
        } else {
            None
        }
    }
}

impl<const N: usize> Default for Point<N> {
    fn default() -> Self {
        Point::from([0.0; N])
    }
}

impl<const N: usize> From<[f64; N]> for Point<N> {
    fn from(components: [f64; N]) -> Self {
        Point { components }
    }
}

impl<const N: usize> Add for Point<N> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let mut newcs = [0.0; N];
        for (ix, (lc, rc)) in self.components().iter().zip(rhs.components()).enumerate() {
            newcs[ix] = lc + rc;
        }
        Point::from(newcs)
    }
}

impl<const N: usize> Sub for Point<N> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        let mut newcs = [0.0; N];
        for (ix, (lc, rc)) in self.components().iter().zip(rhs.components()).enumerate() {
            newcs[ix] = lc - rc;
        }
        Point::from(newcs)
    }
}

impl<const N: usize> Mul for Point<N> {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let mut newcs = [0.0; N];
        for (ix, (lc, rc)) in self.components().iter().zip(rhs.components()).enumerate() {
            newcs[ix] = lc * rc;
        }
        Point::from(newcs)
    }
}

impl<const N: usize> Neg for Point<N> {
    type Output = Self;

    fn neg(self) -> Self::Output {
        let mut newcs = [0.0; N];
        for (ix, c) in self.components().iter().enumerate() {
            newcs[ix] = -c;
        }
        Point::from(newcs)
    }
}

#[cfg(test)]
mod test {
    use super::super::ray::Ray;
    use super::*;

    #[test]
    fn point_defaults_to_origin() {
        let p: Point<3> = Point::default();
        let zeros = [0.0; 3];
        assert_eq!(&zeros, p.components())
    }

    #[test]
    fn point_adds_componentwise() {
        let p1 = Point::from([1.0, 2.0, 3.0]);
        let p2 = Point::from([2.0, 2.0, 2.0]);
        let r = p1 + p2;
        assert_eq!(&[3.0, 4.0, 5.0], r.components())
    }

    #[test]
    fn point_muls_componentwise() {
        let p1 = Point::from([1.0, 2.0, 3.0]);
        let p2 = Point::from([2.0, 2.0, 2.0]);
        let r = p1 * p2;
        assert_eq!(&[2.0, 4.0, 6.0], r.components())
    }

    #[test]
    fn point_subs_componentwise() {
        let p1 = Point::from([1.0, 2.0, 3.0]);
        let p2 = Point::from([2.0, 2.0, 2.0]);
        let r = p1 - p2;
        assert_eq!(&[-1.0, 0.0, 1.0], r.components())
    }

    #[test]
    fn point_negs_componentwise() {
        let p1 = Point::from([1.0, 2.0, 3.0]);
        let r = -p1;
        assert_eq!(&[-1.0, -2.0, -3.0], r.components())
    }

    #[test]
    fn point_surface_norm_returns_none_if_not_self() {
        let p = Point::from([1.0, 1.0]);
        let i = p.norm(&Point::default());
        assert_eq!(None, i)
    }

    #[test]
    fn point_surface_norm_returns_some_if_self() {
        let p = Point::from([1.0, 1.0]);
        let i = p.norm(&Point::from([1.0, 1.0]));
        assert!(i.is_some())
    }

    #[test]
    fn point_surface_intersect_returns_some_if_parallel_to_ray() {
        let p = Point::from([1.0, 1.0]);
        let r = Ray::from((Point::from([-1.0, -1.0]), Vector::from([1.0, 1.0])));
        let i = p.intersect(&r);
        assert_eq!(Some(p), i)
    }

    #[test]
    fn point_surface_intersect_returns_none_if_not_parallel_to_ray() {
        let p = Point::from([1.0, 1.0]);
        let r = Ray::from((Point::from([-2.0, -1.0]), Vector::from([1.0, 1.0])));
        let i = p.intersect(&r);
        assert_eq!(None, i)
    }
}
