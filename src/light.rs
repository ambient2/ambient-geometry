use crate::Ray;

use super::{Color, Point, Vector};

pub struct DirectedLight<const N: usize> {
    location: Point<N>,
    direction: Vector<N>,
    color: Color,
    intensity: f64,
}

impl<const N: usize> DirectedLight<N> {
    pub fn new(
        location: Point<N>,
        direction: Vector<N>,
        color: Color,
        intensity: f64,
    ) -> DirectedLight<N> {
        DirectedLight {
            location,
            direction,
            color,
            intensity,
        }
    }

    pub fn location(&self) -> &Point<N> {
        &self.location
    }

    pub fn direction(&self) -> &Vector<N> {
        &self.direction
    }

    pub fn color(&self) -> &Color {
        &self.color
    }

    pub fn intensity(&self) -> &f64 {
        &self.intensity
    }

    pub fn ray(&self) -> Ray<N> {
        let r = Ray::new(self.location().clone(), self.direction().clone());
        r
    }
}
