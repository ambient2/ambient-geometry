use super::Color;

pub struct VisibleObject<T, const N: usize> {
    surface: T,
    albedo: f64,
    color: Color,
}

impl<T, const N: usize> VisibleObject<T, N> {
    pub fn new(surface: T, color: Color, albedo: f64) -> VisibleObject<T, N> {
        VisibleObject {
            surface,
            color,
            albedo,
        }
    }

    pub fn surface(&self) -> &T {
        &self.surface
    }

    pub fn color(&self) -> &Color {
        &self.color
    }

    pub fn albedo(&self) -> &f64 {
        &self.albedo
    }
}
